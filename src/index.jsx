import React from 'react';
import ReactDOM from 'react-dom';
import Login from './components/Login';
import history from './components/history';
import Cadastro from './components/Cadastro';
import {Route, Router, Switch, Redirect} from 'react-router-dom';



function PrivateRouter({ component: Component }) {
    return (
      <Route
        render={props =>
          localStorage.getItem('test-token') ? (
            <Component {...props} />
          ) : (
            <Redirect to="/" />
          )
        }
      />
    );
  }

ReactDOM.render( 
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={Login} />
                <PrivateRouter path="/cadastro" component={Cadastro} />
            </Switch>
        </Router>
            
, document.getElementById('root'));

