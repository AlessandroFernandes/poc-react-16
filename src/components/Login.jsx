import React, {Fragment} from 'react';
import Grid from '@material-ui/core/Grid';
import Logo from '../img/logo.png';
import { withStyles } from '@material-ui/core/styles'
import history from './history';
import '../styles/i4pro.css';


const styles = theme => ({
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200,
    },
})

class Login extends React.Component {

 
    
    logarSistema(event){
        event.preventDefault();
        let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkFsZXNzYW5kcm8gRmVybmFuZGVzIiwiaWF0IjoidGVzdGUifQ.ADzqlwmZEI6tdPruJXDUQFNZvH6AdYqd0j0-nXGRiwA";
      
        const logar = {
            method: "GET",
           //body: JSON.stringify({nome: this.login.value, password: this.password.value}),
            headers: new Headers({
                "Content-type": "application/json"
            })
        };
             
        fetch('//localhost:5000/user/1', logar)
        .then(response=> {  
                    return response.json()
        })
        .then(response => {
            if(response.nome === this.login.value && response.password === this.password.value){
                localStorage.setItem("test-token", token);
                history.push('/cadastro');
            }else{
                console.log("Erro")
            }
        })

    }

    // eslint-disable-next-line react/no-typos
    render(){
        
        const { classes } = this.props;

        return(
            <Fragment>
                <Grid container className="container">
                    <Grid item xm={12} md={12} lg={12}>
                    <img src={Logo} className="logo_principal"/>
                        <fieldset>
                               <legend>login</legend>
                                <form onSubmit={this.logarSistema.bind(this)} className="login">
                                    <label>Login: teste</label>
                                    <input 
                                        type="text"
                                        placeholder="LOGIN"
                                        ref={(input) => this.login = input} />
                                    <label>Password: teste</label>
                                    <input
                                        placeholder="PASSWORD"
                                        type="password"
                                        ref={(inputp) => this.password = inputp} />

                                    <input type="submit" value="Entrar"></input>
                                    
                                </form>
                        </fieldset>
                    </Grid>               
                </Grid>
            </Fragment>
        );
    }
}
export default withStyles(styles) (Login);