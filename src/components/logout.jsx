import React from 'react';
import history from './history';


export default class Logout extends React.Component {
    
    componentWillUnmount = () => {
        localStorage.removeItem('test-token');
        history.push("/");
    }
    
    render(){
        return null;
    }
}