import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Fab from '@material-ui/core/Fab';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';  
import Add from '@material-ui/icons/Add';
import Avatar from '../img/avata.png';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Delete from '@material-ui/icons/Delete'; 
import InputBase from '@material-ui/core/InputBase';
import Create from '@material-ui/icons/Create'
import { fade } from '@material-ui/core/styles/colorManipulator';



const styles = theme => ({
  text: {
    paddingTop: theme.spacing.unit * 2,
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
  },
  paper: {
    paddingBottom: 50,
  },
  list: {
    marginBottom: theme.spacing.unit * 2,
  },
  subHeader: {
    backgroundColor: theme.palette.background.paper,
  },
  appBar: {
    top: 'auto',
    bottom: 0,
  },
  toolbar: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  fabButton: {
    position: 'absolute',
    zIndex: 1,
    top: -30,
    left: 0,
    right: 0,
    margin: '0 auto',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing.unit * 2,
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 3,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
});


class Cadastro extends React.Component {

  state = {
    open: false,
    lista: [],
    msg: ''
  }
  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  
  componentDidMount(){
    this.buscar();
  }
  componentWillMount(){

  }

  buscar(){
    fetch('//localhost:5000/user')
    .then(resposta => resposta.json())
    .then(resposta => this.setState({ lista : resposta}))
    .catch(Erro => console.log(Erro));
  }

  delete(event, i){
    event.preventDefault();
    let dados = this.state.lista;
    dados.splice(i, 1);
    this.setState({
      lista: dados
    })
  }
  filtrar(event){
    var updateList = this.state.lista;
    updateList = this.state.lista.filter(item => { 
      if (item.nome === event.target.value){
        this.setState({ lista: updateList})
        console.log(this.state.lista.nome);
        console.log('funcionou');
      }else{
        console.log(this.state.lista.nome);
      }
  })
}

  enviar(event){
    event.preventDefault();

    let dados = {
      method: "POST",
      body: JSON.stringify({nome: this.nome.value, email: this.email.value, password: ''}),
      headers: new Headers ({
        "Content-type" : "application/json"
      })
    };
    fetch('//localhost:5000/user', dados)
    .then(response => {
        if(response.ok){
          return this.setState({ msg: `Usuário Adiconado com sucesso!`});
        }else{
          console.log('erro');
          return this.setState({ msg: `Erro ao adicionar Usuário`})
        }
      })
      setTimeout(() => {
        this.setState({ msg: '', });
        this.nome.value = '';
        this.email.value = '';
      },4000);
  }

  

  render(){

    const { classes } = this.props;

    return (
      <React.Fragment>
        <CssBaseline />
        <Paper square className={classes.paper}>
          <Typography className={classes.text} variant="h5" gutterBottom>
            Contato
          </Typography>
          
          <List className={classes.list}>

            {this.state.lista.map(({ id, nome, email}) => (
              <Fragment key={id}>
              
                <ul>
                  <li> 
                      <ListItem button>
                      <img src={Avatar} className="avatar" />
                      <ListItemText primary={nome} secondary={email}  />
                      </ListItem>
                  </li> 
                  <li>
                    <Delete onClick={this.delete.bind(this)}/>
                    <Create />
                  </li>
                </ul>
              </Fragment>
            ))}
            
          </List>
        </Paper>


                <AppBar position="fixed" color="primary" className={classes.appBar}>
                  <Toolbar className={classes.toolbar}>
                    <Fab color="secondary" aria-label="Add" className={classes.fabButton}>
                      <Add  onClick={this.handleOpen.bind(this)}/>
                              <Dialog
                          open={this.state.open}
                          onClose={this.handleClose}
                          aria-labelledby="form-dialog-title"
                          className="enviar-input"

                        >
                          <DialogTitle id="form-dialog-title">Adicionar Novo Contato</DialogTitle>
                          <span>{this.state.msg}</span>
                          <ul>
                            <li><img src={Avatar} className="enviar-esquerda"/></li>
                            <li>
                            <form onSubmit={this.enviar.bind(this)} className="enviar-direita">
                            <label>Nome</label>
                            <input type="text" placeholder="USUÁRIO" ref={(user) => this.nome = user} />

                            <label>Email</label>
                            <input type="email" placeholder="EMAIL" ref={(email) => this.email = email}/>

                            <input type="submit" />
                          </form></li>
                          </ul>
                              
                          <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                              Cancelar
                            </Button>
                          </DialogActions>
                        </Dialog>
            </Fab>
                  <div className={classes.search}>
                    <InputBase
                      placeholder="Buscar"
                      onChange={this.filtrar.bind(this)}
                    />
                  </div>
          </Toolbar>
        </AppBar>
      </React.Fragment>
    );
  }
}

Cadastro.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Cadastro);